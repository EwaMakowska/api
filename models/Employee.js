﻿var mongoose = require('mongoose');

var EmployeeSchema = new mongoose.Schema({
  id: String,
  companyID: String,
  name: String,
  surname: String,
  pin: Number,
  operationNames: [String]
});

module.exports = mongoose.model('Employees', EmployeeSchema);