﻿var mongoose = require('mongoose');

var OperationSchema = new mongoose.Schema({
  id: String,
  companyID: String,
  name: String,
});

module.exports = mongoose.model('Operations', OperationSchema);