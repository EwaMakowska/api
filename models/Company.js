﻿var mongoose = require('mongoose');

var CompanySchema = new mongoose.Schema({
  id: String,
  name: String,
  surname: String,
  phone: String,
  brand: String
});

module.exports = mongoose.model('Companies', CompanySchema);