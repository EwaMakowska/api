var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
require('dotenv').config()

var indexRouter = require('./routes/index');
var wizzardRouter = require('./routes/wizzard');

var mongoose = require('mongoose');

var app = express();

app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/wizzard', wizzardRouter);

const MONGO_URL = process.env["MONGO_URL"];
if (!MONGO_URL) throw new Error("Mongo URL not set");

const MONGO_USER = process.env["MONGO_USER"];
if (!MONGO_USER) throw new Error("Mongo user not set");

const MONGO_PASSWORD = process.env["MONGO_PASSWORD"];
if (!MONGO_PASSWORD) throw new Error("Mongo password not set");

// mongoose.connect('mongodb://localhost/prodio', {
mongoose.connect(MONGO_URL, {
    user: MONGO_USER,
    pass: MONGO_PASSWORD,
    promiseLibrary: require('bluebird'),
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(() =>  console.log('connection successful'))
  .catch((err) => console.error(err));

module.exports = app;
