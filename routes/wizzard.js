﻿var express = require('express');
var router = express.Router();
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var Company = require('../models/Company.js');
var Employee = require('../models/Employee.js');
var Operation = require('../models/Operation.js');

// socket io
io.on('connection', function (socket) {
  socket.on('newdata', function (data) {
    io.emit('new-data', { data: data });
  });
  socket.on('updatedata', function (data) {
    io.emit('update-data', { data: data });
  });
});

router.post('/add', function (req, res, next) {

  var companyID;

  Company.create(req.body.company, function (err, company) {
    companyID = company._id

    for (operation of req.body.operations) {
      operation.companyID = companyID
      for(let employee of req.body.employees){
        employee.companyID = companyID
        for(operationName of employee.operationNames){
          if(operationName === operation.name){
            operationName = operation._id
          }
        }
      }
    }

    Operation.create(req.body.operations, function (err) {
      if (err) {
        console.log(err);
      }
    });

    Employee.create(req.body.employees, function (err) {
      if (err) {
        console.log(err);
      }
    })

    res.json(company)

  });
});

server.listen(5000)
module.exports = router;